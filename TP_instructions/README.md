# Instructions de TPs

Les TPs sont numérotés et peuvent être consultés

- au format Markdown, ex: [tp1](./tp1.md), consultable en IDE,
- au format [Codelabs](https://github.com/googlecodelabs/tools), ex: [tp1](./tp1/index.html), consultable dans votre navigateur web.
